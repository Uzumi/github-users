package ru.popova.test.githubusers.di.components;

import dagger.Subcomponent;
import ru.popova.test.githubusers.activities.fragments.UserFragment;
import ru.popova.test.githubusers.di.modules.UserModule;
import ru.popova.test.githubusers.di.scopes.UserScope;

/** Dagger 2 component for fragment {@link UserFragment}. */
@UserScope
@Subcomponent(modules = {UserModule.class})
public interface UserComponent {

  /**
   * Inject of {@link UserFragment}.
   *
   * @param userFragment - fragment with user description.
   */
  void inject(UserFragment userFragment);
}
