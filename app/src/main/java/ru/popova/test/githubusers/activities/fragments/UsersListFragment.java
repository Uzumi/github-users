package ru.popova.test.githubusers.activities.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.popova.test.githubusers.MainApplication;
import ru.popova.test.githubusers.R;
import ru.popova.test.githubusers.activities.fragments.adapters.UsersListAdapter;
import ru.popova.test.githubusers.models.User;

/** Fragment to view the list of users. */
public class UsersListFragment extends Fragment
    implements UsersListPresenter.Listener, UsersListAdapter.BottomReachedListener {

  @Inject UsersListPresenter usersListPresenter;

  @BindView(R.id.rv_users)
  RecyclerView userRecycler;

  @BindView(R.id.users_list_layout)
  ConstraintLayout usersLayout;

  @BindView(R.id.rv_progress_bar)
  ProgressBar progress;

  private Unbinder unbinder;
  private UsersListAdapter adapter;

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    MainApplication.getApplication(getContext()).getUsersListComponent().inject(this);
    View view = inflater.inflate(R.layout.fragment_users_list, container, false);
    unbinder = ButterKnife.bind(this, view);
    usersListPresenter.bindListener(this);
    userRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
    adapter =
        new UsersListAdapter(
            new ArrayList<>(),
            this,
            (bundle) ->
                Navigation.findNavController(view)
                    .navigate(R.id.action_usersListFragment_to_userFragment, bundle));
    userRecycler.setAdapter(adapter);
    if (usersListPresenter.getUsers().size() > 0) {
      showUsers(usersListPresenter.getUsers());
    } else {
      usersLayout.setVisibility(View.INVISIBLE);
      usersListPresenter.requestUsers(usersListPresenter.getLastUserId());
    }
    return view;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    usersListPresenter.clearDisposables();
    unbinder.unbind();
    usersListPresenter.unbindListener();
    if (getActivity().isFinishing()) {
      MainApplication.getApplication(getContext()).releaseUsersListComponent();
    }
  }

  @Override
  public void showUsers(List<User> users) {
    usersLayout.setVisibility(View.VISIBLE);
    progress.setVisibility(View.INVISIBLE);
    adapter.setItems(users);
    adapter.notifyDataSetChanged();
  }

  @Override
  public void onBottomReached(int position) {
    usersListPresenter.requestUsers(usersListPresenter.getLastUserId());
  }
}
