package ru.popova.test.githubusers.models;

import com.google.gson.annotations.SerializedName;

/** Model of user description. */
public class UserDescription {

  @SerializedName("login")
  private String login;

  @SerializedName("name")
  private String name;

  @SerializedName("avatar_url")
  private String avatarUrl;

  @SerializedName("location")
  private String location;

  @SerializedName("public_repos")
  private String repositories;

  @SerializedName("followers")
  private String followers;

  /** Default constructor. */
  public UserDescription() {
    login = "";
    name = "";
    avatarUrl = "";
    location = "";
    repositories = "";
    followers = "";
  }

  public String getLogin() {
    return login;
  }

  public String getName() {
    return name;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public String getLocation() {
    return location;
  }

  public String getRepositories() {
    return repositories;
  }

  public String getFollowers() {
    return followers;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public void setFollowers(String followers) {
    this.followers = followers;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setRepositories(String repositories) {
    this.repositories = repositories;
  }
}
