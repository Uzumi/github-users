package ru.popova.test.githubusers.activities.fragments;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import ru.popova.test.githubusers.models.UserDescription;
import ru.popova.test.githubusers.services.GitHubUsersService;

/** Used to process GitHub user data. */
public class UserPresenter {

  private GitHubUsersService gitHubUsersService;
  private UserPresenter.Listener listener;
  private CompositeDisposable disposable;

  private UserDescription userDescription;

  /** Default constructor. */
  public UserPresenter() {
    gitHubUsersService = new GitHubUsersService();
    userDescription = new UserDescription();
    disposable = new CompositeDisposable();
  }

  /**
   * Bind interface for display user information.
   *
   * @param listener - interface for display user information.
   */
  public void bindListener(UserPresenter.Listener listener) {
    this.listener = listener;
  }

  /** Unbind interface for display user information. */
  public void unbindListener() {
    this.listener = null;
  }

  /**
   * Obtain information about the user.
   *
   * @param login - user login.
   */
  public void requestUserDescription(String login) {
    disposable.add(
        gitHubUsersService
            .getUser(login)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                userDescription -> {
                  this.userDescription = userDescription;
                  if (listener != null) {
                    listener.showUser(this.userDescription);
                  }
                },
                ex -> ex.printStackTrace()));
  }

  /** Clear {@link CompositeDisposable}. */
  public void clearDisposables() {
    disposable.clear();
  }

  public UserDescription getUserDescription() {
    return userDescription;
  }

  public Listener getListener() {
    return listener;
  }

  /** Designed to display user information. */
  public interface Listener {
    void showUser(UserDescription userDescription);
  }
}
