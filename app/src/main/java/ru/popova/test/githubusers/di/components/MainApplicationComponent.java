package ru.popova.test.githubusers.di.components;

import javax.inject.Singleton;

import dagger.Component;

/** Dagger 2 component for application {@link ru.popova.test.githubusers.MainApplication}. */
@Singleton
@Component
public interface MainApplicationComponent {

  /**
   * The method of creating the component {@link UsersListComponent}.
   *
   * @return - component for fragment {@link
   *     ru.popova.test.githubusers.activities.fragments.UsersListFragment}.
   */
  UsersListComponent createUsersListComponent();

  /**
   * The method of creating the component {@link UserComponent}.
   *
   * @return - component for fragment {@link
   *     ru.popova.test.githubusers.activities.fragments.UserFragment}.
   */
  UserComponent createUserComponent();
}
