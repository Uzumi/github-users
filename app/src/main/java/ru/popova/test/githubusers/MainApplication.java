package ru.popova.test.githubusers;

import android.app.Application;
import android.content.Context;

import ru.popova.test.githubusers.di.components.DaggerMainApplicationComponent;
import ru.popova.test.githubusers.di.components.MainApplicationComponent;
import ru.popova.test.githubusers.di.components.UserComponent;
import ru.popova.test.githubusers.di.components.UsersListComponent;

/** Class for maintaining global application state extends from {@link Application}. */
public class MainApplication extends Application {

  private static MainApplicationComponent mainApplicationComponent;
  private UsersListComponent usersListComponent;
  private UserComponent userComponent;

  @Override
  public void onCreate() {
    super.onCreate();
    mainApplicationComponent = DaggerMainApplicationComponent.create();
  }

  /**
   * Get application method.
   *
   * @param context - an object that provides access to the basic functions of the application.
   * @return - {@link MainApplication} object.
   */
  public static MainApplication getApplication(Context context) {
    return (MainApplication) context.getApplicationContext();
  }

  /**
   * Method of obtaining a {@link UsersListComponent} component.
   *
   * @return - users list component.
   */
  public UsersListComponent getUsersListComponent() {
    if (usersListComponent == null) {
      usersListComponent = mainApplicationComponent.createUsersListComponent();
    }
    return usersListComponent;
  }

  /**
   * Method of obtaining a {@link UsersListComponent} component.
   *
   * @return - user list component/
   */
  public UserComponent getUserComponent() {
    if (userComponent == null) {
      userComponent = mainApplicationComponent.createUserComponent();
    }
    return userComponent;
  }

  /** The method of releasing the UsersList component. */
  public void releaseUsersListComponent() {
    usersListComponent = null;
  }

  /** The method of releasing the User component. */
  public void releaseUserComponent() {
    userComponent = null;
  }
}
