package ru.popova.test.githubusers.api;

import java.util.ArrayList;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.popova.test.githubusers.models.User;
import ru.popova.test.githubusers.models.UserDescription;

/** Interface by Github API methods. */
public interface GitHubApi {

  /**
   * Method to get users.
   *
   * @param id - id starting the data.
   * @return - response with users list.
   */
  @GET("users")
  Single<ArrayList<User>> getUsers(@Query("since") Long id);

  /**
   * Method to get user description.
   *
   * @param login - user login.
   * @return - response with user description.
   */
  @GET("/users/{login}")
  Single<UserDescription> getUser(@Path("login") String login);
}
