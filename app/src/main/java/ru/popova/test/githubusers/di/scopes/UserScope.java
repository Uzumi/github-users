package ru.popova.test.githubusers.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/** Scope of visibility {@link ru.popova.test.githubusers.di.components.UserComponent}. */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface UserScope {}
