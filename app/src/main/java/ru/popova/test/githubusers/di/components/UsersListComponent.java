package ru.popova.test.githubusers.di.components;

import dagger.Subcomponent;
import ru.popova.test.githubusers.activities.fragments.UsersListFragment;
import ru.popova.test.githubusers.di.modules.UsersListModule;
import ru.popova.test.githubusers.di.scopes.UsersListScope;

/** Dagger 2 component for fragment {@link UsersListFragment}. */
@UsersListScope
@Subcomponent(modules = {UsersListModule.class})
public interface UsersListComponent {

  /**
   * Inject of {@link UsersListFragment}.
   *
   * @param usersListFragment - fragment with a list of users of GitHub.
   */
  void inject(UsersListFragment usersListFragment);
}
