package ru.popova.test.githubusers.activities.fragments;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import ru.popova.test.githubusers.models.User;
import ru.popova.test.githubusers.services.GitHubUsersService;

/** Used to get a list of GitHub users */
public class UsersListPresenter {

  private GitHubUsersService gitHubUsersService;
  private UsersListPresenter.Listener listener;
  private CompositeDisposable disposable;

  private List<User> users;
  private long lastUserId;

  /** Default constructor. */
  public UsersListPresenter() {
    users = new ArrayList<>();
    gitHubUsersService = new GitHubUsersService();
    lastUserId = 0;
    disposable = new CompositeDisposable();
  }

  /**
   * Bind interface to display a list of users.
   *
   * @param listener - interface to display a list of users.
   */
  void bindListener(UsersListPresenter.Listener listener) {
    this.listener = listener;
  }

  /** Unbind interface to display a list of users. */
  void unbindListener() {
    this.listener = null;
  }

  public List<User> getUsers() {
    return users;
  }

  /**
   * Obtain information about list of users.
   *
   * @param since - id starting the data.
   */
  public void requestUsers(Long since) {
    disposable.add(
        gitHubUsersService
            .getUsers(since)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                users -> {
                  this.lastUserId = users.get(users.size() - 1).getId();
                  this.users.addAll(users);
                  if (listener != null) {
                    listener.showUsers(this.users);
                  }
                },
                ex -> ex.printStackTrace()));
  }

  /** Clear {@link CompositeDisposable}. */
  public void clearDisposables() {
    disposable.clear();
  }

  public Listener getListener() {
    return listener;
  }

  Long getLastUserId() {
    return lastUserId;
  }

  /** Designed to display a list of users. */
  public interface Listener {
    void showUsers(List<User> users);
  }
}
