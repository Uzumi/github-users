package ru.popova.test.githubusers.activities.fragments.interfaces;

import android.os.Bundle;

/** Interface for processing a click on an item RecyclerView. */
public interface RecyclerViewClickListener {

  /**
   * Event click on the item.
   *
   * @param bundle - bundle with user login.
   */
  void onItemClick(Bundle bundle);
}
