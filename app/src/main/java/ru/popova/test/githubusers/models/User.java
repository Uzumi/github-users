package ru.popova.test.githubusers.models;

import com.google.gson.annotations.SerializedName;

/** Model of user data. */
public class User {

  @SerializedName("login")
  private String login;

  @SerializedName("id")
  private Long id;

  @SerializedName("avatar_url")
  private String avatarUrl;

  /** Default constructor. */
  public User() {
    login = "";
    id = null;
    avatarUrl = "";
  }

  public Long getId() {
    return id;
  }

  public String getLogin() {
    return login;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
