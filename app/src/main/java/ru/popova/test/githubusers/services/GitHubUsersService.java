package ru.popova.test.githubusers.services;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.ArrayList;

import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.popova.test.githubusers.BuildConfig;
import ru.popova.test.githubusers.api.GitHubApi;
import ru.popova.test.githubusers.models.User;
import ru.popova.test.githubusers.models.UserDescription;

/** Service for getting information about GitHub users. */
public class GitHubUsersService {

  private GitHubApi gitHubApi;

  /** Default constructor. */
  public GitHubUsersService() {
    Retrofit retrofit =
        new Retrofit.Builder()
            .baseUrl(BuildConfig.GITHUB_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    gitHubApi = retrofit.create(GitHubApi.class);
  }

  /**
   * The method of obtaining a list of users.
   *
   * @param id - id starting the data.
   * @return - response with users list.
   */
  public Single<ArrayList<User>> getUsers(Long id) {
    return gitHubApi.getUsers(id);
  }

  /**
   * The method of obtaining information about the user.
   *
   * @param login - user login.
   * @return - response with user description.
   */
  public Single<UserDescription> getUser(String login) {
    return gitHubApi.getUser(login);
  }
}
