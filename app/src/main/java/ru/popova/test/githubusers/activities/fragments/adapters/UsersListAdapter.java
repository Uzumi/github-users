package ru.popova.test.githubusers.activities.fragments.adapters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import ru.popova.test.githubusers.R;
import ru.popova.test.githubusers.activities.fragments.interfaces.RecyclerViewClickListener;
import ru.popova.test.githubusers.models.User;

/** Adapter for user list. */
public class UsersListAdapter extends RecyclerView.Adapter<UsersListAdapter.UsersListHolder> {

  private List<User> users;
  private RecyclerViewClickListener listener;
  private BottomReachedListener bottomReachedListener;

  /**
   * Constructor with parameters.
   *
   * @param users - users list.
   * @param bottomReachedListener - end-of-list processing interface.
   * @param listener - item click handling interface.
   */
  public UsersListAdapter(
      List<User> users,
      BottomReachedListener bottomReachedListener,
      RecyclerViewClickListener listener) {
    this.users = users;
    this.listener = listener;
    this.bottomReachedListener = bottomReachedListener;
  }

  @Override
  public int getItemCount() {
    return users.size();
  }

  @NonNull
  @Override
  public UsersListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    return new UsersListHolder(
        LayoutInflater.from(viewGroup.getContext())
            .inflate(R.layout.users_list_item, viewGroup, false));
  }

  @Override
  public void onBindViewHolder(@NonNull UsersListHolder usersListHolder, int i) {
    usersListHolder.itemView.setOnClickListener(
        (view) -> {
          Bundle bundle = new Bundle();
          bundle.putString("login", users.get(i).getLogin());
          listener.onItemClick(bundle);
        });
    usersListHolder.bindItem(
        users.get(i).getLogin(), users.get(i).getId(), users.get(i).getAvatarUrl());
    if (i == users.size() - 1) {
      bottomReachedListener.onBottomReached(i);
    }
  }

  public void setItems(List<User> users) {
    this.users = users;
  }

  /** Class for working with list items */
  class UsersListHolder extends RecyclerView.ViewHolder {

    TextView loginView, idView;
    CircularImageView avatarView;

    /**
     * Constructor with parameters.
     *
     * @param itemView - view fot item.
     */
    UsersListHolder(@NonNull View itemView) {
      super(itemView);
      ButterKnife.bind(itemView);
      loginView = itemView.findViewById(R.id.cv_user_login);
      idView = itemView.findViewById(R.id.cv_user_id);
      avatarView = itemView.findViewById(R.id.cv_user_avatar);
    }

    /**
     * Filling in the item list.
     *
     * @param title - user login.
     * @param id - user id.
     * @param avatarUrl - user avatar URL.
     */
    public void bindItem(String title, Long id, String avatarUrl) {
      Picasso.get().load(avatarUrl).into(avatarView);
      loginView.setText(title);
      idView.setText("id: " + id);
    }
  }

  /** Used for event handling - end of list reached. */
  public interface BottomReachedListener {
    void onBottomReached(int position);
  }
}
