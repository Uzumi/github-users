package ru.popova.test.githubusers.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/** Scope of visibility {@link ru.popova.test.githubusers.di.components.UsersListComponent}. */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface UsersListScope {}
