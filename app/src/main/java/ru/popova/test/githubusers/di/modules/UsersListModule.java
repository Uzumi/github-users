package ru.popova.test.githubusers.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.popova.test.githubusers.activities.fragments.UsersListPresenter;
import ru.popova.test.githubusers.di.scopes.UsersListScope;

/**
 * Dagger 2 module for for component {@link
 * ru.popova.test.githubusers.di.components.UsersListComponent}.
 */
@Module
public class UsersListModule {

  /**
   * Providing a {@link UsersListPresenter}.
   *
   * @return - presenter for fragment {@link
   *     ru.popova.test.githubusers.activities.fragments.UsersListFragment}.
   */
  @UsersListScope
  @Provides
  UsersListPresenter provideUsersListPresenter() {
    return new UsersListPresenter();
  }
}
