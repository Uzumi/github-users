package ru.popova.test.githubusers.activities.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.popova.test.githubusers.MainApplication;
import ru.popova.test.githubusers.R;
import ru.popova.test.githubusers.models.UserDescription;

/** Fragment to view user description. */
public class UserFragment extends Fragment implements UserPresenter.Listener {

  @Inject UserPresenter userPresenter;

  @BindViews({
    R.id.user_login,
    R.id.user_name,
    R.id.user_location,
    R.id.user_repos,
    R.id.user_followers
  })
  List<TextView> textViewList;

  @BindViews({
    R.id.user_login_text,
    R.id.user_name_text,
    R.id.user_location_text,
    R.id.ic_repos,
    R.id.ic_followers
  })
  List<View> textDescription;

  @BindView(R.id.user_avatar)
  CircularImageView avatarView;

  @BindView(R.id.user_layout)
  ConstraintLayout userLayout;

  @BindView(R.id.progress)
  ProgressBar progress;

  private Unbinder unbinder;

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    MainApplication.getApplication(getContext()).getUserComponent().inject(this);
    userPresenter.bindListener(this);
    View view = inflater.inflate(R.layout.fragment_user, container, false);
    unbinder = ButterKnife.bind(this, view);
    return view;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    if (userPresenter.getUserDescription().getLogin().equals(getArguments().getString("login"))) {
      showUser(userPresenter.getUserDescription());
    } else {
      userLayout.setVisibility(View.INVISIBLE);
      userPresenter.requestUserDescription(getArguments().getString("login"));
    }
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    userPresenter.clearDisposables();
    unbinder.unbind();
    userPresenter.unbindListener();
    if (getActivity().isFinishing()) {
      MainApplication.getApplication(getContext()).releaseUserComponent();
    }
  }

  /**
   * Method to display user information.
   *
   * @param i - index for {@link List<TextView>}.
   * @param text - informational text.
   */
  private void setText(int i, String text) {
    if (text != null) {
      textViewList.get(i).setText(text);
    } else {
      textViewList.get(i).setVisibility(View.INVISIBLE);
      textDescription.get(i).setVisibility(View.INVISIBLE);
    }
  }

  @Override
  public void showUser(UserDescription userDescription) {
    userLayout.setVisibility(View.VISIBLE);
    progress.setVisibility(View.INVISIBLE);
    Picasso.get().load(userDescription.getAvatarUrl()).into(avatarView);
    setText(0, userDescription.getLogin());
    setText(1, userDescription.getName());
    setText(2, userDescription.getLocation());
    setText(3, userDescription.getRepositories());
    setText(4, userDescription.getFollowers());
  }
}
