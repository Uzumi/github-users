package ru.popova.test.githubusers.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.popova.test.githubusers.activities.fragments.UserPresenter;
import ru.popova.test.githubusers.di.scopes.UserScope;

/**
 * Dagger 2 module for for component {@link ru.popova.test.githubusers.di.components.UserComponent}.
 */
@Module
public class UserModule {

  /**
   * Providing a {@link ru.popova.test.githubusers.activities.fragments.UserPresenter}.
   *
   * @return - presenter for fragment {@link
   *     ru.popova.test.githubusers.activities.fragments.UserFragment}.
   */
  @Provides
  @UserScope
  UserPresenter provideUserPresenter() {
    return new UserPresenter();
  }
}
