package ru.popova.test.githubusers.activities.fragments;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class UsersListPresenterTest {

  @Rule public MockitoRule rule = MockitoJUnit.rule();

  @Mock UsersListPresenter.Listener listener;

  private UsersListPresenter presenter;

  @Before
  public void before() {
    presenter = new UsersListPresenter();
  }

  @Test
  public void bindListener() {
    assertNull(presenter.getListener());
    presenter.bindListener(listener);
    assertNotNull(presenter.getListener());
  }

  @Test
  public void unbindListener() {
    assertNull(presenter.getListener());
    presenter.bindListener(listener);
    presenter.unbindListener();
    assertNull(presenter.getListener());
  }
}
