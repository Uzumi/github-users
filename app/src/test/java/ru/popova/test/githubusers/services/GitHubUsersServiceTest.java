package ru.popova.test.githubusers.services;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;

import io.reactivex.observers.TestObserver;
import ru.popova.test.githubusers.models.User;
import ru.popova.test.githubusers.models.UserDescription;

public class GitHubUsersServiceTest {

  public static final String GITHUB_FIRST_USER = "mojombo";

  @Rule public MockitoRule rule = MockitoJUnit.rule();

  private GitHubUsersService gitHubUsersService;

  @Before
  public void before() {
    gitHubUsersService = new GitHubUsersService();
  }

  @Test
  public void getUsers() {
    TestObserver<ArrayList<User>> observer = gitHubUsersService.getUsers(0l).test();
    observer.assertValueAt(0, users -> users.get(0).getLogin().equals(GITHUB_FIRST_USER));
  }

  @Test
  public void getUser() {
    TestObserver<UserDescription> observer = gitHubUsersService.getUser(GITHUB_FIRST_USER).test();
    observer.assertValueAt(
        0, userDescription -> userDescription.getLogin().equals(GITHUB_FIRST_USER));
  }
}
