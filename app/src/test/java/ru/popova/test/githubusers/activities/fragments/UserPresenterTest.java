package ru.popova.test.githubusers.activities.fragments;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

public class UserPresenterTest {

  @Rule public MockitoRule rule = MockitoJUnit.rule();

  @Mock UserPresenter.Listener listener;

  private UserPresenter presenter;

  @Before
  public void before() {
    doNothing().when(listener).showUser(any());
    presenter = new UserPresenter();
  }

  @Test
  public void bindListener() {
    assertNull(presenter.getListener());
    presenter.bindListener(listener);
    assertNotNull(presenter.getListener());
  }

  @Test
  public void unbindListener() {
    assertNull(presenter.getListener());
    presenter.bindListener(listener);
    presenter.unbindListener();
    assertNull(presenter.getListener());
  }
}
